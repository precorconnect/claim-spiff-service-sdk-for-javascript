import {Container} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import ClaimSpiffServiceSdkConfig from './claimSpiffServiceSdkConfig';
import AddClaimSpiffFeature from './addClaimSpiffFeature';
import ListClaimSpiffFeature from './listclaimSpiffFeature';
import UpdatePartnerRepFeature from './updatePartnerRepFeature';
import ListSpiffLogsFeature from './listSpiffLogsFeature';
import ListSpiffLogsWithIdFeature from './listSpiffLogsWithIdFeature';
import UpdateSpiffLogForApproveFeature from './updateSpiffLogForApproveFeature';
import UpdateSpiffLogForRejectFeature from './updateSpiffLogForRejectFeature';
import GetSpiffEntitlementWithPartnerSaleRegIdFeature from './getSpiffEntitlementWithPartnerSaleRegIdFeature';
import BackFillSpiffDetailsFeature from './backFillSpiffDetailsFeature';
/**
 * @class {DiContainer}
 */
export default class DiContainer {

    _container:Container;

    /**
     * @param {ClaimSpiffServiceSdkConfig} config
     */
    constructor(config:ClaimSpiffServiceSdkConfig) {

        if (!config) {
            throw 'config required';
        }

        this._container = new Container();

        this._container.registerInstance(ClaimSpiffServiceSdkConfig, config);
        this._container.autoRegister(HttpClient);

        this._registerFeatures();

    }

    /**
     * Resolves a single instance based on the provided key.
     * @param key The key that identifies the object to resolve.
     * @return Returns the resolved instance.
     */
    get(key:any):any {
        return this._container.get(key);
    }

    _registerFeatures() {

        this._container.autoRegister(AddClaimSpiffFeature);
        this._container.autoRegister(ListClaimSpiffFeature);
        this._container.autoRegister(UpdatePartnerRepFeature);
        this._container.autoRegister(ListSpiffLogsFeature);
        this._container.autoRegister(ListSpiffLogsWithIdFeature);
        this._container.autoRegister(UpdateSpiffLogForApproveFeature);
        this._container.autoRegister(UpdateSpiffLogForRejectFeature);
        this._container.autoRegister(GetSpiffEntitlementWithPartnerSaleRegIdFeature);
        this._container.autoRegister(BackFillSpiffDetailsFeature);

    }

}
