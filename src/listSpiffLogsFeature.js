import {inject} from 'aurelia-dependency-injection';
import ClaimSpiffServiceSdkConfig from './claimSpiffServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import SpiffLogWebView from './spiffLogWebView';

@inject(ClaimSpiffServiceSdkConfig, HttpClient)
class ListSpiffLogsFeature {

    _config:ClaimSpiffServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:ClaimSpiffServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Lists Spiff Logs
     * @param {string} accessToken
     * @returns {Promise.<SpiffLogWebView[]>}
     */
    execute(accessToken:string):Promise<SpiffLogWebView[]> {

        return this._httpClient
            .createRequest(`spiff-logs`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .catch(error => {
                console.log(error);
            })
            .then(response => {
                   return Array.from(
                        response.content,
                        contentItem =>
                            new SpiffLogWebView(
                                contentItem.claimId,
                                contentItem.partnerSaleRegistrationId,
                                contentItem.partnerAccountId,
                                contentItem.partnerRepUserId,
                                contentItem.sellDate,
                                contentItem.installDate,
                                contentItem.spiffAmount,
                                contentItem.spiffClaimedDate,
                                contentItem.facilityName,
                                contentItem.invoiceNumber,
                                contentItem.reviewStatus,
                                contentItem.reviewedBy,
                                contentItem.reviewedDate,
                                contentItem.partnerName,
                                contentItem.partnerRepName
                            )
                    );
            });
    }
}

export default ListSpiffLogsFeature;
