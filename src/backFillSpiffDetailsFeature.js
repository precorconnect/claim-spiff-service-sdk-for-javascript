import {inject} from 'aurelia-dependency-injection';
import ClaimSpiffServiceSdkConfig from './claimSpiffServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'

@inject(ClaimSpiffServiceSdkConfig, HttpClient)
class BackFillSpiffDetailsFeature {

    _config:ClaimSpiffServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:ClaimSpiffServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * @param {string} accessToken
     */
    execute(accessToken:string){

        return this._httpClient
            .createRequest(`/claim-spiffs/backfillspiffdetails`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
    }
}

export default BackFillSpiffDetailsFeature;
