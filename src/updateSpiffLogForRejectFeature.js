import {inject} from 'aurelia-dependency-injection';
import ClaimSpiffServiceSdkConfig from './claimSpiffServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';

@inject(ClaimSpiffServiceSdkConfig, HttpClient)
class UpdateSpiffLogForRejectFeature {

    _config:ClaimSpiffServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:ClaimSpiffServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * @param id
     * @param accessToken
     * @returns {void|Promise<any>|*}
     */
    execute(id:number,
            accessToken:string){

        return this._httpClient
            .createRequest(`/spiff-logs/${id}/reject`)
            .asPut()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
    }

}

export default UpdateSpiffLogForRejectFeature;