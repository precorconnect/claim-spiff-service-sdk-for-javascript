/**
 * @class {SpiffLogWebView}
 */
export default class SpiffLogWebView {

    _claimId:number;

    _partnerSaleRegistrationId:number;

    _partnerAccountId:string;

    _partnerRepUserId:string;

    _sellDate:string;

    _installDate:string;

    _spiffAmount:number;

    _spiffClaimedDate:string;

    _facilityName:string;

    _invoiceNumber:string;

    _reviewStatus:string;

    _reviewedBy:string;

    _reviewedDate:string;

    _partnerName:string;

    _partnerRepName:string;

    /**
     * @param {number} claimId
     * @param {number} partnerSaleRegistrationId
     * @param {string} partnerAccountId
     * @param {string} partnerRepUserId
     * @param {string} sellDate
     * @param {string} installDate
     * @param {double} spiffAmount
     * @param {string} spiffClaimedDate
     * @param {string} facilityName
     * @param {string} invoiceNumber
     * @param {string} reviewStatus
     * @param {string} reviewedBy
     * @param {string} reviewedDate
     * @param {string} partnerName
     * @param {string} partnerRepName
     */
    constructor(claimId:number,
                partnerSaleRegistrationId:number,
                partnerAccountId:string,
                partnerRepUserId:string,
                sellDate:string,
                installDate:string,
                spiffAmount:number,
                spiffClaimedDate:string,
                facilityName:string,
                invoiceNumber:string,
                reviewStatus:string,
                reviewedBy:string,
                reviewedDate:string,
                partnerName:string,
                partnerRepName:string
    ) {

        if (!claimId) {
            throw new TypeError('claimId required');
        }
        this._claimId = claimId;

        if (!partnerSaleRegistrationId) {
            throw new TypeError('partnerSaleRegistrationId required');
        }
        this._partnerSaleRegistrationId = partnerSaleRegistrationId;

        if (!partnerAccountId) {
            throw new TypeError('partnerAccountId required');
        }
        this._partnerAccountId = partnerAccountId;

        if (!partnerRepUserId) {
            throw new TypeError('partnerRepUserId required');
        }
        this._partnerRepUserId = partnerRepUserId;

        if(!sellDate){
            throw new TypeError('sellDate required');
        }
        this._sellDate = sellDate;

        this._installDate = installDate;

        if (!spiffAmount) {
            throw new TypeError('spiffAmount required');
        }
        this._spiffAmount = spiffAmount;

        if (!spiffClaimedDate) {
            throw new TypeError('spiffClaimedDate required');
        }
        this._spiffClaimedDate = spiffClaimedDate;

        if (!facilityName) {
            throw new TypeError('facilityName required');
        }
        this._facilityName = facilityName;

        if (!invoiceNumber) {
            throw new TypeError('invoiceNumber required');
        }
        this._invoiceNumber = invoiceNumber;

        this._reviewStatus = reviewStatus;

        this._reviewedBy = reviewedBy;

        this._reviewedDate = reviewedDate;

        if (!partnerName) {
            throw new TypeError('partnerName required');
        }
        this._partnerName = partnerName;

        if (!partnerRepName) {
            throw new TypeError('partnerRepName required');
        }
        this._partnerRepName = partnerRepName;
    }

    /**
     * @returns {number}
     */
    get claimId():number {
        return this._claimId;
    }

    /**
     * @returns {number}
     */
    get partnerSaleRegistrationId():number {
        return this._partnerSaleRegistrationId;
    }

    /**
     * @returns {string}
     */
    get partnerAccountId():string {
        return this._partnerAccountId;
    }

    /**
     * @returns {string}
     */
    get partnerRepUserId():string {
        return this._partnerRepUserId;
    }

    /**
     * @returns {string}
     */
    get sellDate():string{
        return this._sellDate;
    }

    /**
     * @returns {string}
     */
    get installDate():string {
        return this._installDate;
    }

    /**
     * @returns {number}
     */
    get spiffAmount():number {
        return this._spiffAmount;
    }

    /**
     * @returns {string}
     */
    get spiffClaimedDate():string {
        return this._spiffClaimedDate;
    }

    /**
     * @returns {string}
     */
    get facilityName():string {
        return this._facilityName;
    }

    /**
     * @returns {string}
     */
    get invoiceNumber():string {
        return this._invoiceNumber;
    }

    /**
     * @returns {string}
     */
    get reviewStatus():string {
        return this._reviewStatus;
    }

    /**
     * @returns {string}
     */
    get reviewedBy():string {
        return this._reviewedBy;
    }

    /**
     * @returns {string}
     */
    get reviewedDate():string {
        return this._reviewedDate;
    }

    /**
     * @returns {string}
     */
    get partnerName():string {
        return this._partnerName;
    }

    /**
     * @returns {string}
     */
    get partnerRepName():string {
        return this._partnerRepName;
    }

    toJSON() {
        return {
            claimId : this._claimId,
            partnerSaleRegistrationId: this._partnerSaleRegistrationId,
            partnerAccountId: this._partnerAccountId,
            partnerRepUserId: this._partnerRepUserId,
            sellDate:this._sellDate,
            installDate: this._installDate,
            spiffAmount: this._spiffAmount,
            spiffClaimedDate: this._spiffClaimedDate,
            facilityName: this._facilityName,
            invoiceNumber: this._invoiceNumber,
            reviewStatus:this._reviewStatus,
            reviewedBy:this._reviewedBy,
            reviewedDate:this._reviewedDate,
            partnerName:this._partnerName,
            partnerRepName:this._partnerRepName
        };
    }
}