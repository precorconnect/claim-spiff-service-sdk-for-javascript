/**
 * @class {ClaimSpiffSynopsisView}
 */
export default class ClaimSpiffSynopsisView {

    _claimId:number;

    _partnerSaleRegistrationId:number;

    _partnerAccountId:string;

    _partnerRepUserId:string;

    _installDate:string;

    _spiffAmount:number;

    _spiffClaimedDate:string;

    _facilityName:string;

    _invoiceNumber:string;


    /**
     * @param {number} claimId
     * @param {number} partnerSaleRegistrationId
     * @param {string} partnerAccountId
     * @param {string} partnerRepUserId
     * @param {string} installDate
     * @param {double} spiffAmount
     * @param {string} spiffClaimedDate
     * @param {string} facilityName
     * @param {string} invoiceNumber
     */
    constructor(claimId:number,
                partnerSaleRegistrationId:number,
                partnerAccountId:string,
                partnerRepUserId:string,
                installDate:string,
                spiffAmount:number,
                spiffClaimedDate:string,
                facilityName:string,
                invoiceNumber:string) {

        if (!claimId) {
            throw new TypeError('claimId required');
        }
        this._claimId = claimId;

        if (!partnerSaleRegistrationId) {
            throw new TypeError('partnerSaleRegistrationId required');
        }
        this._partnerSaleRegistrationId = partnerSaleRegistrationId;

        if (!partnerAccountId) {
            throw new TypeError('partnerAccountId required');
        }
        this._partnerAccountId = partnerAccountId;

        if (!partnerRepUserId) {
            throw new TypeError('partnerRepUserId required');
        }
        this._partnerRepUserId = partnerRepUserId;

        this._installDate = installDate;

        if (!spiffAmount) {
            throw new TypeError('spiffAmount required');
        }
        this._spiffAmount = spiffAmount;

        if (!spiffClaimedDate) {
            throw new TypeError('spiffClaimedDate required');
        }
        this._spiffClaimedDate = spiffClaimedDate;

        if (!facilityName) {
            throw new TypeError('facilityName required');
        }
        this._facilityName = facilityName;

        if (!invoiceNumber) {
            throw new TypeError('invoiceNumber required');
        }
        this._invoiceNumber = invoiceNumber;

    }

    /**
     * @returns {number}
     */
    get claimId():number {
        return this._claimId;
    }

    /**
     * @returns {number}
     */
    get partnerSaleRegistrationId():number {
        return this._partnerSaleRegistrationId;
    }

    /**
     * @returns {string}
     */
    get partnerAccountId():string {
        return this._partnerAccountId;
    }

    /**
     * @returns {string}
     */
    get partnerRepUserId():string {
        return this._partnerRepUserId;
    }

    /**
     * @returns {string}
     */
    get installDate():string {
        return this._installDate;
    }

    /**
     * @returns {number}
     */
    get spiffAmount():number {
        return this._spiffAmount;
    }

    /**
     * @returns {string}
     */
    get spiffClaimedDate():string {
        return this._spiffClaimedDate;
    }

    /**
     * @returns {string}
     */
    get facilityName():string {
        return this._facilityName;
    }

    /**
     * @returns {string}
     */
    get invoiceNumber():string {
        return this._invoiceNumber;
    }

    toJSON() {
        return {
            claimId : this._claimId,
            partnerSaleRegistrationId: this._partnerSaleRegistrationId,
            partnerAccountId: this._partnerAccountId,
            partnerRepUserId: this._partnerRepUserId,
            installDate: this._installDate,
            spiffAmount: this._spiffAmount,
            spiffClaimedDate: this._spiffClaimedDate,
            facilityName: this._facilityName,
            invoiceNumber: this._invoiceNumber
        };
    }
}