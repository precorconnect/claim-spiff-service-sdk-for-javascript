import {inject} from 'aurelia-dependency-injection';
import ClaimSpiffServiceSdkConfig from './claimSpiffServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import AddClaimSpiffReq from './addClaimSpiffReq';

@inject(ClaimSpiffServiceSdkConfig, HttpClient)
class AddClaimSpiffFeature {

    _config:ClaimSpiffServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:ClaimSpiffServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * Adds a claim spiffs
     * @param {AddClaimSpiffReq} request
     * @param {string} accessToken
     * @returns {Promise.<number>} claimIds
     */
    execute(request:AddClaimSpiffReq[], accessToken:string):Promise<number> {

        return this._httpClient
            .createRequest('claim-spiffs')
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then(response => response.content);
    }
}

export default AddClaimSpiffFeature;