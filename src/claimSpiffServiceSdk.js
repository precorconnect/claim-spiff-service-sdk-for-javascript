import ClaimSpiffServiceSdkConfig from './claimSpiffServiceSdkConfig';
import DiContainer from './diContainer';
import ClaimSpiffWebDto from './addClaimSpiffReq';
import AddClaimSpiffFeature from './addClaimSpiffFeature';
import ClaimSpiffSynopsisView from './claimSpiffSynopsisView';
import ListClaimSpiffFeature from './listclaimSpiffFeature';
import UpdatePartnerRepFeature from './updatePartnerRepFeature';
import SpiffLogWebView from './spiffLogWebView';
import ListSpiffLogsFeature from './listSpiffLogsFeature';
import ListSpiffLogsWithIdFeature from './listSpiffLogsWithIdFeature';
import UpdateSpiffLogForApproveFeature from './updateSpiffLogForApproveFeature';
import UpdateSpiffLogForRejectFeature from './updateSpiffLogForRejectFeature';
import SpiffEntitlementWebView from  './spiffEntitlementWebView';
import GetSpiffEntitlementWithPartnerSaleRegIdFeature from './getSpiffEntitlementWithPartnerSaleRegIdFeature';
import BackFillSpiffDetailsFeature from './backFillSpiffDetailsFeature';


/**
 * @class {ClaimSpiffServiceSdk}
 */
export default class ClaimSpiffServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {ClaimSpiffServiceSdkConfig} config
     */
    constructor(config:ClaimSpiffServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    /**
     * Update the partner rep
     * @param {number} partnerSaleRegistrationId
     * @param {string} partnerRepUserId
     * @param {string} accessToken
     * @returns {Promise.<void>}
     */
    updatePartnerRep(partnerSaleRegistrationId:number,
                     partnerRepUserId:string,
                     accessToken:string):Promise<string> {

        this
            ._diContainer
            .get(UpdatePartnerRepFeature)
            .execute(
                partnerSaleRegistrationId,
                partnerRepUserId,
                accessToken
            );

    }

    /**
     * Adds a claim spiffs
     * @param {AddClaimSpiffReq} request
     * @param {string} accessToken
     * @returns {Promise.<number>} claimIds
     */
    addClaimSpiffs(request:ClaimSpiffWebDto,
                  accessToken:string):Promise<string> {

        return this
            ._diContainer
            .get(AddClaimSpiffFeature)
            .execute(
                request,
                accessToken
            );

    }

    /**
     * @param {string[]} claimIds
     * @param {string} accessToken
     * @returns {Array<ClaimSpiffSynopsisView>}
     */
    getClaimSpiffsWithIds(claimIds:string[],accessToken:string):Promise<ClaimSpiffSynopsisView[]> {

        return this
            ._diContainer
            .get(ListClaimSpiffFeature)
            .execute(
                claimIds,
                accessToken
            );

    }


    /**
     * @param {string} accessToken
     * @returns {SpiffLogWebView}
     */
    listSpiffLogs(accessToken:string):Promise<SpiffLogWebView[]>{

        return this
            ._diContainer
            .get(ListSpiffLogsFeature)
            .execute(accessToken);

    }

    /**
     *
     * @param partnerSaleRegistrationId
     * @param accessToken
     * @returns {SpiffEntitlementWebView}
     */
    getEntitlementWithPartnerSaleRegistrationId(partnerSaleRegistrationId:number,
                                                accessToken:string):Promise<SpiffEntitlementWebView> {

        return this
            ._diContainer
            .get(GetSpiffEntitlementWithPartnerSaleRegIdFeature)
            .execute(
                partnerSaleRegistrationId,
                accessToken
            );

    }

    /**
     * @param {string} accountId
     * @param {string} accessToken
     * @returns {SpiffLogWebView}
     */
    listSpiffLogsWithId(accountId:string,accessToken:string):Promise<SpiffLogWebView>{

        return this
            ._diContainer
            .get(ListSpiffLogsWithIdFeature)
            .execute(
                accountId,
                accessToken
            );

    }

    /**
     * @param {number} id
     * @param {string} accessToken
     */
    updateSpiffLogForApprove(id:number,accessToken:string){

        return this
            ._diContainer
            .get(UpdateSpiffLogForApproveFeature)
            .execute(
                id,
                accessToken
            );

    }

    /**
     *
     * @param {number} id
     * @param {string} accessToken
     * @returns {Promise<void>}
     */
    updateSpiffLogForReject(id:number,accessToken:string){

        return this
            ._diContainer
            .get(UpdateSpiffLogForRejectFeature)
            .execute(
                id,
                accessToken
            );

    }

    /**
     * @param accessToken
     * @returns {*}
     */
    backFillSpiffDetails(accessToken:string){

        return this
            ._diContainer
            .get(BackFillSpiffDetailsFeature)
            .execute(
                accessToken
            );

    }

}
