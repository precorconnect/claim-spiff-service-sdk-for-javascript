import ClaimSpiffSynopsisView from '../../src/claimSpiffSynopsisView';
import dummy from '../dummy';

describe('ClaimSpiffSynopsisView', ()=> {
    describe('constructor', () => {
        it('throws if claimId is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new ClaimSpiffSynopsisView(
                    null,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'claimId required');
        });
        it('sets claimId', () => {
            /*
             arrange
             */
            const expectedId = 1;

            /*
             act
             */
            const objectUnderTest =
                new ClaimSpiffSynopsisView(
                    expectedId,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             assert
             */
            const actualId = objectUnderTest.claimId;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if partnerSaleRegistrationId is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new ClaimSpiffSynopsisView(
                    dummy.claimId,
                    null,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'partnerSaleRegistrationId required');
        });
        it('sets partnerSaleRegistrationId', () => {
            /*
             arrange
             */
            const expectedId = 1;

            /*
             act
             */
            const objectUnderTest =
                new ClaimSpiffSynopsisView(
                    dummy.claimId,
                    expectedId,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             assert
             */
            const actualId = objectUnderTest.partnerSaleRegistrationId;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if partnerAccountId is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new ClaimSpiffSynopsisView(
                    dummy.claimId,
                    dummy.partnerSaleRegistrationId,
                    null,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'partnerAccountId required');
        });
        it('sets partnerAccountId', () => {
            /*
             arrange
             */
            const expectedId = "1";

            /*
             act
             */
            const objectUnderTest =
                new ClaimSpiffSynopsisView(
                    dummy.claimId,
                    dummy.partnerSaleRegistrationId,
                    expectedId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             assert
             */
            const actualId = objectUnderTest.partnerAccountId;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if partnerRepUserId is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new ClaimSpiffSynopsisView(
                    dummy.claimId,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    null,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'partnerRepUserId required');
        });
        it('sets partnerRepUserId', () => {
            /*
             arrange
             */
            const expectedId = "12345";

            /*
             act
             */
            const objectUnderTest =
                new ClaimSpiffSynopsisView(
                    dummy.claimId,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    expectedId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             assert
             */
            const actualId = objectUnderTest.partnerRepUserId;
            expect(actualId).toEqual(expectedId);

        });
        it('does not throw if installDate is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new ClaimSpiffSynopsisView(
                    dummy.claimId,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    null,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             act/assert
             */
            expect(constructor).not.toThrow()
        });
        it('sets installDate', () => {
            /*
             arrange
             */
            const expectedId = "1";

            /*
             act
             */
            const objectUnderTest =
                new ClaimSpiffSynopsisView(
                    dummy.claimId,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    expectedId,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             assert
             */
            const actualId = objectUnderTest.installDate;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if spiffAmount is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new ClaimSpiffSynopsisView(
                    dummy.claimId,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    null,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'spiffAmount required');
        });
        it('sets spiffAmount', () => {
            /*
             arrange
             */
            const expectedId = 1;

            /*
             act
             */
            const objectUnderTest =
                new ClaimSpiffSynopsisView(
                    dummy.claimId,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    expectedId,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             assert
             */
            const actualId = objectUnderTest.spiffAmount;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if spiffClaimedDate is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new ClaimSpiffSynopsisView(
                    dummy.claimId,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    null,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'spiffClaimedDate required');
        });
        it('sets spiffClaimedDate', () => {
            /*
             arrange
             */
            const expectedId = "1";

            /*
             act
             */
            const objectUnderTest =
                new ClaimSpiffSynopsisView(
                    dummy.claimId,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    expectedId,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             assert
             */
            const actualId = objectUnderTest.spiffClaimedDate;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if facilityName is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new ClaimSpiffSynopsisView(
                    dummy.claimId,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    null,
                    dummy.invoiceNumber
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'facilityName required');
        });
        it('sets facilityName', () => {
            /*
             arrange
             */
            const expectedId = "Precor";

            /*
             act
             */
            const objectUnderTest =
                new ClaimSpiffSynopsisView(
                    dummy.claimId,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    expectedId,
                    dummy.invoiceNumber
                );

            /*
             assert
             */
            const actualId = objectUnderTest.facilityName;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if invoiceNumber is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new ClaimSpiffSynopsisView(
                    dummy.claimId,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    null
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'invoiceNumber required');
        });
        it('sets invoiceNumber', () => {
            /*
             arrange
             */
            const expectedId = "1";

            /*
             act
             */
            const objectUnderTest =
                new ClaimSpiffSynopsisView(
                    dummy.claimId,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    expectedId
                );

            /*
             assert
             */
            const actualId = objectUnderTest.invoiceNumber;
            expect(actualId).toEqual(expectedId);

        });
    })
});
