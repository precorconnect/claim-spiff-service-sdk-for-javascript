import SpiffEntitlementWebView  from '../../src/spiffEntitlementWebView';
import dummy from '../dummy';

describe('SpiffEntitlementWebView', ()=> {
    describe('constructor', () => {
        it('does not throw if partnerRepUserId is null',() => {
            /**
             arrange
             */
            const constructor = () =>
                new SpiffEntitlementWebView(
                    null,
                    dummy.spiffAmount
                );

            /**
             * act/assert
             */
            expect(constructor).not.toThrow()
        });
        it('sets partnerRepUserId',()=>{
            /**
             arrange
             */
            const expectedPartnerRepUserId = dummy.partnerRepUserId;

            /**
             * act
             */
            const objectUnderTest =
                new SpiffEntitlementWebView(
                    expectedPartnerRepUserId,
                    dummy.spiffAmount
                );

            /**
             * act
             */
            const actualPartnerRepUserId
                = objectUnderTest.partnerRepUserId;

            expect(actualPartnerRepUserId).toEqual(expectedPartnerRepUserId);
        });
        it('throws if spiffAmount is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new SpiffEntitlementWebView(
                    dummy.partnerRepUserId,
                    null
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'spiffAmount required');
        });
        it('sets spiffAmount', () => {
            /*
             arrange
             */
            const expectedSpiffAmount = dummy.spiffAmount;

            /*
             act
             */
            const objectUnderTest =
                new SpiffEntitlementWebView(
                    dummy.partnerRepUserId,
                    expectedSpiffAmount
                );

            /*
             assert
             */
            const actualSpiffAmount = objectUnderTest.spiffAmount;
            expect(actualSpiffAmount).toEqual(expectedSpiffAmount);

        });
    })
});
