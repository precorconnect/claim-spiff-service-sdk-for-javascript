import ClaimSpiffServiceSdk,{
    AddClaimSpiffReq,
    SpiffEntitlementWebView
} from '../../src/index';
import config from './config';
import factory from './factory';
import dummy from '../dummy';

describe('Index module', () => {

    describe('default export', () => {
        it('should be ClaimSpiffServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new ClaimSpiffServiceSdk(config.claimSpiffServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(ClaimSpiffServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('updatePartnerRep method', () => {
            it('should updatePartnerRep', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new ClaimSpiffServiceSdk(config.claimSpiffServiceSdkConfig);

                /*
                 act
                 */
                const actPromise =
                    objectUnderTest
                        .updatePartnerRep(
                            dummy.partnerSaleRegistrationId,
                            dummy.partnerRepUserId,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        );

                /*
                 assert
                 */
                actPromise
                    .then((result) => {
                        expect(result=='Updated successfully').toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));
            },20000);
        });
        describe('listSpiffLogs method',() => {
            it('should list SpiffLogWebView more than 1', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new ClaimSpiffServiceSdk(config.claimSpiffServiceSdkConfig);

                /**
                 * act
                 */
                const actPromise =
                    objectUnderTest.listSpiffLogs(
                        factory.constructValidPartnerRepOAuth2AccessToken()
                    );

                /**
                 * assert
                 */
                actPromise
                    .then((SpiffLogWebView) => {
                        expect(SpiffLogWebView.length >= 1).toBeTruthy();
                        done();
                    }
                ).catch(error => done.fail(JSON.stringify(error)));
            },20000);
        });
        describe('listSpiffLogsWithId method', () => {
            it('should return SpiffLogWebView more than 1', (done) => {

                /**
                 * arrange
                 */
                const objectUnderTest =
                    new ClaimSpiffServiceSdk(config.claimSpiffServiceSdkConfig);


                /**
                 * act
                 */
                const actPromise =
                    objectUnderTest.listSpiffLogsWithId(
                        dummy.partnerAccountId,
                        factory.constructValidPartnerRepOAuth2AccessToken()
                    ).catch(error => done.fail(JSON.stringify(error)));

                /**
                 * assert
                 */
                actPromise
                    .then(SpiffLogWebView =>{
                        expect(SpiffLogWebView.length >= 1).toBeTruthy();
                        done();
                    }).catch(error => done.fail(JSON.stringify(error)));
            },20000);
        });
        describe('updateSpiffLogForApprove method',() => {
           it('should approve Spiff Log', (done) => {

               /**
                * arrange
                */
               const objectUnderTest =
                   new ClaimSpiffServiceSdk(config.claimSpiffServiceSdkConfig);

               const actPromise =
                   objectUnderTest
                       .listSpiffLogsWithId(
                            dummy.partnerAccountId,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                       );

               const claimIdPromise =
               actPromise
                       .then(SpiffLogWebView => {
                           return Array.from(SpiffLogWebView,
                                    SpiffLogWebView => {
                                        //console.log('SpiffLogWebView:',SpiffLogWebView._claimId);
                                        return SpiffLogWebView._claimId;
                                    }
                                )
                            }
                       )
                       .then((claimId) =>{
                              return claimId;
                            }
                       );

               claimIdPromise
                   .then(claimId => {
                        objectUnderTest
                            .updateSpiffLogForApprove(
                                claimId[0],
                                factory.constructValidPartnerRepOAuth2AccessToken()
                            ).catch(error => done.fail(JSON.stringify(error)));
                        }
                    );

               /**
                * assert
                */
               actPromise
                   .then(() =>{
                       //expect(response.statusCode).toBe(200);
                        done();
                       }
                   ).catch(error => done.fail(JSON.stringify(error)));
           })
        });
        describe('getSpiffEntitlementWithPartnerSaleRegId method',() => {
            it('should return SpiffEntitlementWebView', (done) => {

                /**
                 * arrange
                 */
                const objectUnderTest =
                    new ClaimSpiffServiceSdk(config.claimSpiffServiceSdkConfig);

                const actPromise =
                    objectUnderTest
                        .getEntitlementWithPartnerSaleRegistrationId(
                            dummy.partnerSaleRegistrationId,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        );

                /**
                 * assert
                 */
                actPromise
                    .then((SpiffEntitlementWebView) =>{
                            expect(SpiffEntitlementWebView).toBeTruthy();
                            done();
                        }
                    ).catch(error => done.fail(JSON.stringify(error)));
            })
        });
    });
});